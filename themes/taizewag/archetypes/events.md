---
title: "{{ replace .Name "-" " " | title }}"
date: {{ getenv "DATE" | dateFormat "2006-01-02" }}
image: "/uploads/header1.jpg"
dinner: false
draft: false
publishDate: 2021-01-01
---

You're invited to join the Taizé prayer. More information via [Spectrum Wageningen](https://spectrum-wageningen.com/)

