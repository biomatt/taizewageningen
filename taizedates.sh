#https://www.unix.com/unix-for-dummies-questions-and-answers/259042-how-find-second-fourth-monday-month.html
# date's %u format specifier supplies the day-of-week of the first day in the target month, which then is subtracted from 16 (for second week) or 30 (4. wk, experimental values). Should the First be a Monday, subtract another 7 days.
#YEAR=`date '+%Y'`
YEAR=2025
for MONTH in {01..06}; do
#    for DAY in {16,30}; do
     export DATE=$(printf "%4s-%2s-%02d" $YEAR $MONTH $(($(date -d"${YEAR}${MONTH}01" +"16 - %u - (%u==1?7:0)"))))
     echo ${DATE}
     hugo new --kind events events/${DATE}.md
#    done
done
