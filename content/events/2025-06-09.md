---
title: "2025 06 09"
date: 2025-06-09
image: "/uploads/header5.jpg"
dinner: false
draft: false
publishDate: 2021-01-01
---

You're invited to join the Taizé prayer. More information via [Spectrum Wageningen](https://spectrum-wageningen.com/)

