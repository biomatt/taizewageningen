---
title: "European Meeting Taizé in Wageningen"
date: 2020-12-27T20:00:00.000Z
endDate: 2021-01-01T16:39:12.644Z
image: "/uploads/header3.jpg"
dinner: false
draft: false
publishDate: 2020-01-01
---
[Nederlandse tekst zie [hieronder](#europese-ontmoeting-taizé)]

In Wageningen, we organise a Taizé meeting during the Christmas Holidays. Meet people from all over the world, pray together, have small bible groups and follow workshops. It will be local and international at the same time. Live in Wageningen + connected to Taizé in France. 

*This year, due to the corona crisis, the European Meeting that was planned to be held in Turin (Italy) cannot take place there. Yet, we are happy to announce a totally new initiative: This year, you can participate in the European Meeting everywhere, from December 27th until January 1st. Part of the programme will be broadcasted from Taizé and from elsewhere. And, if the corona rules allow, you can meet in smaller or bigger groups to be able to participate in the meeting together and share your experiences with one another.* – Brothers Jasper & Sebastiaan (2 Dutch brothers of Taizé).

In Wageningen, [Spectrum](https://spectrum-wageningen.com) and some churches are organising a local European Taizé Meeting. We want to invite young people from different churches and students to participate in the prayers, study the bible and moreover, meet one another.

The program will look as follows:

From 28th of Dec – Jan 1st daily:

- 10:30 AM Start with coffee and tea
- 11:00 AM Bible introduction and sharing in small groups
- 12:30 Midday prayer
- 1:00 PM Lunch break – bring your own lunch, go for a walk or eat lunch at home
- 2:30 PM Workshops (program varies per day, live workshops + workshops streamed from Taizé)
- 8:30 PM: Evening Prayer

There is a maximum number of participants in the live meeting. If there will be more than the maximum, new people will be on a waiting list. We will try to find a way that can host as many people as possible.

The different parts of the program will take place in the Bevrijdingskerk Wageningen. Naturally, we will take into account the corona rules.

You can subsribe for the live meeting in Wageningen via the form here. If for some reason you prefer to follow the meeting only only, we advise you to subscribe to the digital Taizé meeting, via the Taizé website. This is only possible until the 10th of December. This way you can participate in an international bible study group. More information can be found on the [Taizé website](https://www.taize.fr/en_article28597.html)


## Registration
{{< rawhtml >}}
<pretix-widget event="https://pretix.eu/taizewageningen/xkl7z/"></pretix-widget>
<noscript>
   <div class="pretix-widget">
        <div class="pretix-widget-info-message">
            JavaScript is disabled in your browser. To access our ticket shop without JavaScript, please <a target="_blank" rel="noopener" href="https://pretix.eu/taizewageningen/xkl7z/">click here</a>.
        </div>
    </div>
</noscript>
{{< /rawhtml >}}

### FAQ
**What is the main target group?**

Young people, 16-35 y

**What is the language spoken during the meeting?**

Mainly English, with Dutch translation if necessary.

**I’d like to help!**

Great! Send an email to [info@taizewageningen.nl](mailto:info@taizedummywageningen.nl)

**When can I subscribe?**

Now! Click the register link above.

**What if meetings won’t be allowed because of Covid?**

The meeting is also [online for a big part](https://www.taize.fr/en_article28597.html)

**Because of Covid, I don’t feel comfortable with the idea of a live meeting. But I do like the idea.**

Check the [website of Taizé for the digital options](https://www.taize.fr/en_article28597.html)!


# Europese Ontmoeting Taizé

Tijdens de kerstvakantie organiseren we in Wageningen een Taizé ontmoeting. Ontmoet mensen uit de hele wereld, bid samen, ontmoet elkaar in  bijbelgroepjes en volg workshops. Het is èn lokaal èn internationaal tegelijkertijd. Live in Wageningen + gelinkt aan Taizé in Frankrijk. 

*Dit jaar zorgt de coronacrisis ervoor dat we de Europese Ontmoeting, die in Turijn zou worden gehouden, daar niet kan worden gehouden. We zijn echter verheugd dat we een geheel nieuw initiatief kunnen aankondigen. Dit jaar kun je de Europese Ontmoeting van 27 december tot 1 januari overal meemaken, waar je ook bent! Gedeelten van het programma worden vanuit Taizé en elders uitgezonden. Als de coronamaatregelen dit toelaten, kun je in kleinere of grotere groepjes bij elkaar komen zodat je samen de ontmoeting kunt bijwonen en je ervaring met anderen kunt delen.* - Broeders Jasper en Sebastiaan

In samenwerking met [Spectrum](https://spectrum-wageningen.com) en verschillende kerken willen we ook in Wageningen de lokale Europese Taizé Ontmoeting vormgeven. We willen de jongeren uit de verschillende kerken en studenten uitnodigen om samen de gebeden mee te maken, de Bijbel te bestuderen en bovenal elkaar te ontmoeten. 

Het programma ziet er ongeveer als volgt uit:

Vanaf 28 dec tot 1 januari dagelijks

- 10:30 Dagopening en koffie en thee
- 11:00 Bijbelintroductie en bespreken in kleine groepen
- 12:30 Middaggebed
- 13:00 Lunch pauze. Neem je eigen lunch mee, maak een wandeling of ga naar huis om thuis te lunchen.
- 14:30 Workshop (het programma varieert van dag tot dag: live workshops en workshops gestreamd vanuit Taizé)
- 20:30 Avondgebed

Er is een maximum aantal deelnemers aan de live ontmoeting. Als er meer opgaven zijn dan dat maximum worden extra mensen op een wachtlijst geplaats. We doen ons best om een manier te vinden waarin zoveel mogelijk mensen deel kunnen nemen.

De verschillende programmaonderdelen zullen plaatsvinden in de Bevrijdingskerk. Uiteraard met inachtneming van de dan geldende coronamaatregelen.

Je kunt je inschrijven voor de live ontmoeting in Wageningen via het formulier hieronder. Als je om de een of andere reden liever alleen wilt/kunt deelnemen aan de online meeting, dan kun je je beter inschrijven via de Taizé website. Dan kun je ook in een digitaal internationaal groepje worden geplaatst. Daar kun je je tot 10 december inschrijven. Voor meer informatie zie de [Taizé website](https://www.taize.fr/en_article28597.html)

## Registratie
{{< rawhtml >}}
<pretix-widget event="https://pretix.eu/taizewageningen/xkl7z/"></pretix-widget>
<noscript>
   <div class="pretix-widget">
        <div class="pretix-widget-info-message">
            JavaScript is disabled in your browser. To access our ticket shop without JavaScript, please <a target="_blank" rel="noopener" href="https://pretix.eu/taizewageningen/xkl7z/">click here</a>.
        </div>
    </div>
</noscript>
{{< /rawhtml >}}

## FAQ

**Wat is de belangrijkste doelgroep?**

Jonge mensen, 16-35 jaar.

**Welke taal wordt gesproken tijdens de ontmoeting?**

Vooral Engels, en als nodig wordt vertaald in Nederlands.

**Ik wil wel helpen!**

Leuk! Mail even naar [info@taizewageningen.nl](mailto:info@taizedummywageningen.nl)

**Wanneer kan ik me inschrijven?**

Nu! Klik op de registratie link hierboven.

**Wat als er geen ontmoeting mag ivm Covid?**

De ontmoeting is ook voor [een groot deel online](https://www.taize.fr/en_article28597.html)

**Ivm Covid wil ik eigenlijk niet live ontmoeting, maar het idee spreekt me wel aan.**

Kijk op de site van Taizé naar de [digitale opties](https://www.taize.fr/en_article28597.html)
