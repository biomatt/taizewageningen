---
title: "Links"
date: 2019-12-07T12:30:07+01:00
---

- [Official website Taizé in France](https://www.taize.fr/)
- [Student Platform & Chaplaincy Wageningen (Spectrum)](https://spectrum-wageningen.com/)

## Other nearby Taizé prayers
- [Ede](http://www.taize-ede.nl) (every 2nd sunday of the month)
- [Bennekom](https://www.facebook.com/TaizeBennekom/events/) (2x per year)
- [Utrecht](http://www.taizeinutrecht.nl/) (monthly)
- [Amersfoort](https://www.facebook.com/pg/Taizeamersfoort/events/)
- [Arnhem](https://www.facebook.com/pg/taize.arnhem/events/)
