---
title: "About us"
date: 2019-12-07T11:04:49+01:00
draft: false
---

The Taizé prayer is organised by [Spectrum](https://spectrum-wageningen.com/) and takes place every 2nd Monday of the Month in the Arboretum Church, August Faliseweg 22, Wageningen.
